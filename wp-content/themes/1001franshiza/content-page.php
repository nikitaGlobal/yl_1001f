<div class="<?php if(!is_page() && !is_single()){ echo 'blog-block-anonse'; } else { echo 'blog-block'; } ?> padding-15 margin-bottom-30">
	<?php		
		the_title( '<h1 class="margin-bottom-20">', '</h1>' );	
		getHeaderBlogImage($post);	
		the_content();	
		//echo getPostViews(get_the_ID());
	?>
	<?php if(is_page() || is_single()) :?>
	<div class="share">
		<p class="nomargin margin-bottom-10">поделиться</p>
		<div data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,linkedin,lj,viber,whatsapp" class="ya-share2 text-right"></div>
	</div>
	<?php endif; ?>	
</div>

<?php if(is_single()): ?>
<?php	
	setPostViews($post->ID);

	$categories = wp_get_post_categories( $post->ID );

	$args =  array(
	'posts_per_page'  =>  '-1',
	'post_type'  =>  'post',
	'numberposts' => 5,	
	'post_status' => 'publish',
	'category__in' => $categories,
	'post__not_in' => array($post->ID),
	'orderby' => 'meta_value_num',
	'order' => 'DESC', 
	'meta_key' => 'post_views_count', 	
	);	

	$popularposts = new WP_Query( $args  );
	 ?>
	<?php if($popularposts->have_posts()) : ?>
		<div class="blog-block padding-15">
		<h3 class="margin-bottom-10">Популярные статьи по теме</h3>
		<?php while ( $popularposts->have_posts() ) : $popularposts->the_post();  ?>	
			<a class="other-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br/>			  
		<?php endwhile; ?>
		</div>
	<?php endif; ?>

	

	
	<?php wp_reset_postdata(); ?>

<?php endif; ?>
