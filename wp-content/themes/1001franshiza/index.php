<?php
	get_header();
?>

	<!-- BLOG -->
	
	<div class="container-fluid bg-image-blog box-shadow-big z-index-100 overflow">
		<div class="container nopadding padding-top-40">
			<?php if( is_category() ){ ?>
				<div class="row nomargin">
					<div class="col-xs-12 col-sm-8 col-sm-push-4 margin-bottom-30">					
						<h1><?php echo single_term_title(); ?></h1>									
					</div>						
				</div>
			<?php } ?>	
			<div class="row nomargin">

				<!-- blog -->

				<div class="col-xs-12 col-sm-8 col-sm-push-4 margin-bottom-30">					
					<?php
						$nodata = false;						
						if ( have_posts() ) :
							// Start the Loop.
							while ( have_posts() ) : the_post();															
								get_template_part( 'content', 'page' );
							endwhile;
							// Previous/next page navigation.
							my_paging_nav();	
						else :
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );
							$nodata = true;
						endif;
					?>

				</div>

				<div class="col-xs-12 col-sm-4 col-sm-pull-8 nopadding">
					<?php if (!$nodata) : ?>
					<div class="xs-mobile-12 col-xs-6 col-sm-12">
						<div class="sidebar-block margin-bottom-30">
							<div class="title">Поиск</div>
							<div class="content">
								<form role="search" class="form-horizontal" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
	          						<div class="form-group nomargin">
	            						<input type="text" placeholder="что ищем?" class="form-control search-input" value="<?php echo get_search_query() ?>" name="s" id="s">
	            						<button class="form-control search-button" type="submit" id="searchsubmit">
	            							<i class="fa fa-search"></i>
	            						</button>
	          						</div>	          						
	        					</form>													
							</div>							
						</div>	
					</div>
					<?php endif; ?>


					<div class="xs-mobile-12 col-xs-6 col-sm-12">
						<div class="sidebar-block margin-bottom-30">
							<div class="title">Рубрики</div>
								<ul class="ul-clear sidebar-links">
									<li><a href="<?php echo get_permalink('2'); ?>">Все статьи</a></li>
								<!-- sidebar -->
								<?php  
								$args = array(	
								'orderby' => 'name',	
								'order'   => 'ASC'						
								);
								$categories = get_categories( $args );
								if( $categories ){
									foreach( $categories as $cat ){
										echo '<li><a href="'.get_category_link( $cat->term_id ).'">'.$cat->name.'</a></li>';
									}
								}
								?>	
								</ul>
						</div>	
					</div>
					
					<div class="xs-mobile-12 col-xs-6 col-sm-12">
						<div class="sidebar-block margin-bottom-30">
							<div class="title">Получить бесплатную консультацию</div>
							<div class="padding-15 text-center">
								<img src="<?php echo get_template_directory_uri(); ?>/img/real-phone.jpg" alt="" class="img-responsive margin-bottom-10">
								<a href="#contact_form_pop" class="fancybox-inline btn btn-primary _btn-lg w-100">Получить</a> 
							</div>						
						</div>
					</div>					

					<div class="xs-mobile-12 col-xs-6 col-sm-12 text-center z-index-100 margin-bottom-30">
						<h3 class="margin-bottom-10">Наша КОМПАНИЯ<br>РАБОТАЕТ В СФЕРЕ<br>ФРАНЧАЙЗИНГА</h3>
						<img src="<?php echo get_template_directory_uri(); ?>/img/5years.gif" alt="">
					</div>
				</div>

			</div>			
			<div class="star-1-blog"></div>
			<?php if (!$nodata) : ?>
			<div class="star-2-blog"></div>
			<?php endif; ?>
		</div>
	</div>

	<!-- FOOTER -->
	
<?php
	get_footer();
?>
