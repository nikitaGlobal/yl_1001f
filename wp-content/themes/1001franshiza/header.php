<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
	
    <link href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" rel="stylesheet">

    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css" rel="stylesheet">          

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	   <script src="<?php echo get_template_directory_uri(); ?>/js/less.js" type="text/javascript"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	    (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
	            try {
	                w.yaCounter37050080 = new Ya.Metrika({
	                    id:37050080,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true
	                });
	            } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
	            s = d.createElement("script"),
	            f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = "https://mc.yandex.ru/metrika/watch.js";

	        if (w.opera == "[object Opera]") {
	            d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
	    })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/37050080" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
</head>

<body data-spy="scroll" data-target="#navbar-scroll" data-offset="150">
<!-- CONTENT -->

	<!-- #begin -->

	<div class="str">
		<span class="fa fa-arrow-up"></span> 	
	</div>
	
	<div class="container-fluid header" id="begin">
		<div class="container padding-top-20">
			<div class="logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-responsive"  width="98" alt="">	
				</a>				
			</div>

			<div class="components">				
				<div class="phones margin-bottom-10">						
					<a href="#contact_form_pop" class="fancybox-inline  btn btn-primary w-100 margin-bottom-10">Заказать обратный звонок</a>					
					<div style="display:none" class="fancybox-hidden">
					    <div id="contact_form_pop" class="hentry" style="width:320px;max-width:100%">
					       <?php echo do_shortcode('[contact-form-7 id="105" title="Обратный звонок"]'); ?>
					    </div>
					</div>

					<div class="cont">
						<div class="time">с 9:00 до 19:00</div>
						<div class="numbers">
							<div class="ico"><i class="fa fa-phone"></i></div>
							<span>
								<?php if ( is_active_sidebar( 'contact-header' ) ) : ?>							
								<?php dynamic_sidebar( 'contact-header' ); ?>							
								<?php endif; ?>
							</span>
						</div>	
					</div>								
				</div>	
				
				<!-- nav menu -->
				<nav class="navbar navbar-default">
				  <div>
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>	      
				    </div>


				 <?php
		            wp_nav_menu( array(
		                'menu'              => 'primary',
		                'theme_location'    => 'primary',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		       			'container_id'      => 'bs-example-navbar-collapse-1',
		                'menu_class'        => 'nav navbar-nav navbar-right',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
		        ?>
		        </div>
			   </nav>
			</div>
			
			<!-- <img src="img/top-decor.jpg" alt="" class="decor">	 -->
			
		</div>		
	</div>
<?php 

	if( !is_page_template('landing-page.php')  ){ 
		if ( function_exists('yoast_breadcrumb') ) 
			yoast_breadcrumb('<div class="container-fluid bg-white box-shadow z-index-400">
				<div class="container nopadding">
				<p class="breadcrumb">',
				'</p></div></div>');	
	} 
?>


			
		