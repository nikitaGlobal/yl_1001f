<div class="container-fluid bg-white">
		<div class="container nopadding footer padding-height-40">
			<div class="row nomargin">			
				<div class="col-xs-12 col-sm-8 col-sm-push-4 text-right sm-text-center margin-bottom-20 z-index-400" >
					<?php wp_nav_menu( array( 'container_class' => 'footer-menu ul-clear', 'theme_location' => 'footer_menu' ) ); ?> 
				</div> 
				<div class="col-xs-12 col-sm-4 col-sm-pull-8 sm-text-center z-index-400">
					<div class="social">
						<p>мы в социальных сетях</p>
						<ul class="ul-clear icons">
							<li><a href="" title="Facebook"><span class="fa fa-facebook fa-lg"></span></a></li>
							<li><a href="" title="Linked in"><span class="fa fa-linkedin fa-lg"></span></a></li>
							<li><a href="" title="Instagram"><span class="fa fa-instagram fa-lg"></span></a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="row nomargin padding-lr-20">
				<div class="line"></div>	
			</div>
			
			<div class="row nomargin">
				<div class="xs-mobile-12 xs-reset-push-pull col-xs-6 col-xs-push-6 xs-text-center text-right z-index-400">Создание сайта - <a href="http://www.magazinweb.ru" target="_blank">magazinweb.ru</a></div>
				<div class="xs-mobile-12 xs-reset-push-pull col-xs-6 col-xs-pull-6 xs-text-center z-index-400"><?php echo date('Y'); ?> | 1001franshiza</div>
			</div>
			<div class="footer-decor"></div>
		</div>
	</div>
	



	


	
<!-- END CONTENT -->
	
<!-- INCLUDE SCRIPTS -->
	
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>    
	<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script> 
	<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
	

<!-- END INCLUDE SCRIPTS -->
<?php /*var_dump( $wpdb->queries );  */?>
</body>
</html>
<?php wp_footer(); ?>

