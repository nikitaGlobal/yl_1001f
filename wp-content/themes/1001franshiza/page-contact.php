<?php
/*
Template Name:  Контакты
*/
?>

<?php
	get_header();
?>

<div class="container-fluid bg-image-contact box-shadow-big z-index-100" id="contact">
		<div class="container padding-height-40">
			<h1 class="margin-bottom-30 text-center">Контакты</h1>
			<iframe class="bg-white box-shadow-big margin-bottom-30" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4490.985313545651!2d37.750903397962226!3d55.749943615725556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414acaa8cdcf5825%3A0xc3e9db1ff5463dc8!2z0K3Qu9C10LrRgtGA0L7QtNC90LDRjyDRg9C7LiwgMTAsINCc0L7RgdC60LLQsCwgMTExNTI0!5e0!3m2!1sru!2sru!4v1478765999629" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>					
			<div class="row">
				<div class="xs-mobile-12 col-xs-6 col-sm-6 text-center contact-info margin-bottom-30">
					<?php if ( is_active_sidebar( 'landing-band-10' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-10' ); ?>							
						<?php endif; ?>	
				</div>
				<div class="xs-mobile-12 col-xs-6 col-sm-6">
					<?php echo do_shortcode('[contact-form-7 id="104" title="Форма Контакты"]'); ?>
				</div>
			</div>
		</div>
	</div>

<?php
	if( !is_page_template('landing-page.php')  ){ get_footer(); }
?>