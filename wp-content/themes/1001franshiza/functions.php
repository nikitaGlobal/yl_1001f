<?php
//add_theme_support( 'post-thumbnails' ); //Включение поддержки миниатюр для всех типов записей

add_filter('show_admin_bar', '__return_false');

//remove_filter( 'the_content', 'wpautop' ); // Отключаем автоформатирование в полном посте
//remove_filter( 'the_excerpt', 'wpautop' ); // Отключаем автоформатирование в кратком(анонсе) посте
//remove_filter('comment_text', 'wpautop'); // Отключаем автоформатирование в комментариях

//remove_filter('the_content','wptexturize'); // Отключаем автоформатирование в полном посте
//remove_filter('the_excerpt','wptexturize'); // Отключаем автоформатирование в кратком(анонсе) посте
//remove_filter('comment_text', 'wptexturize'); // Отключаем автоформатирование в комментариях

add_action('widgets_init', 'my_remove_recent_comments_style');
function my_remove_recent_comments_style() {
  global $wp_widget_factory;
  remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

//Отключение показа версии
function wpbeginner_remove_version() {
return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

// Отключение rss-ленты
function fb_disable_feed() {
wp_redirect(get_option('siteurl'));
}

add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);

remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );

//Отключаем ненужные виджеты
// отключить виджеты
function unregister_default_widgets() {
unregister_widget('WP_Widget_Meta');
unregister_widget('WP_Widget_Recent_Comments');
unregister_widget('WP_Widget_RSS');
}
add_action('widgets_init', 'unregister_default_widgets', 11);


function partners_init() {
    $labels = array(
      'name' => 'Партнеры', 
      'singular_name' => 'Партнера',
      'add_new' => 'Добавить',
      'add_new_item'=> 'Новый партнер',
      'edit_item' => 'Изменить',
      'new_item' => 'Новый партнер',
      'all_items' => 'Все партнеры',
      'view_item' => 'Просмотр',
      'search_items' => 'Найти',
      'not_found' => 'Не найдено',
      'not_found_in_trash' => 'Не найдено',
      'parent_item_colon'  =>  '',
      'menu_name'  =>  'Партнеры'
      );

    $args = array(
      'public' => false,
      'publicly_queriable' => true, 
      'show_ui' => true, 
      'exclude_from_search' => true, 
      'show_in_nav_menus' => false,
      'rewrite' => false,
      'has_archive'  => false,
      'labels'  => $labels,
      'supports'  => array('title'),     
      'menu_icon'   => 'dashicons-groups',  
      'description' =>'Партнеры на главной странице'
    );

    register_post_type( 'partners', $args );
}
add_action( 'init', 'partners_init' );

function comments_init() {
    $labels = array(
      'name' => 'Отзывы', 
      'singular_name' => 'Отзыв',
      'add_new' => 'Добавить',
      'add_new_item'=> 'Новый отзыв',
      'edit_item' => 'Изменить',
      'new_item' => 'Новый отзыв',
      'all_items' => 'Все отзывы',
      'view_item' => 'Просмотр',
      'search_items' => 'Найти',
      'not_found' => 'Не найдено',
      'not_found_in_trash' => 'Не найдено',
      'parent_item_colon'  =>  '',
      'menu_name'  =>  'Отзывы'
      );

    $args = array(
      'public' => true,
      'publicly_queriable' => true, 
      'show_ui' => true, 
      'exclude_from_search' => false, 
      'show_in_nav_menus' => false,
      'rewrite' => false,
      'has_archive'  => true,
      'labels'  => $labels,
      'supports'  => array('title'),     
      'menu_icon'   => 'dashicons-admin-customizer',  
      'description' =>'Отзывы на главной странице'
    );

    register_post_type( 'comments', $args );
}
add_action( 'init', 'comments_init' );

//Custom fields

//Партнеры, Отзывы
function fr_include_myuploadscript() {
  // у вас в админке уже должен быть подключен jQuery, если нет - раскомментируйте следующую строку:
  // wp_enqueue_script('jquery');
  // дальше у нас идут скрипты и стили загрузчика изображений WordPress
  if ( ! did_action( 'wp_enqueue_media' ) ) {
    wp_enqueue_media();
  }
  // само собой - меняем admin.js на название своего файла
  wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/js/admin.js', array('jquery'), null, false );
}
 
add_action( 'admin_enqueue_scripts', 'fr_include_myuploadscript' );

function fr_image_uploader_field( $name, $value = '', $w = 134, $h = 134, $default='') {
  
  if( $value ) {
    $image_attributes = wp_get_attachment_image_src( $value, array($w, $h) );
    $src = $image_attributes[0];
  } else {
    $src = $default;
  }
  echo '
  <div>
    <img data-src="' . $default . '" src="' . $src . '" width="' . $w . 'px" height="' . $h . 'px" />
    <div>
      <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
      <button type="submit" class="upload_image_button button">Загрузить</button>
      <button type="submit" class="remove_image_button button">&times;</button>
    </div>
  </div>
  ';
}

function fr_meta_boxes() {
  add_meta_box('fr_image', 'Логотип (134x134 px)', 'fr_image_field_print', 'partners', 'normal', 'high');

  add_meta_box('post', 'Должность', 'fr_post_field_print', 'comments', 'normal', 'high');  
  add_meta_box('fr_image', 'Фото (150x150 px)', 'fr_image_field_print2', 'comments', 'normal', 'high');  
  add_meta_box('post_text', 'Отзыв', 'fr_post_text_area', 'comments', 'normal', 'high');   
  add_meta_box('fr_image', 'Изображение в заголовке (675x380 px)', 'fr_image_field_print3', 'post', 'normal', 'high');  
}
 
add_action('admin_menu', 'fr_meta_boxes');
 

function fr_image_field_print($post) {
  if( function_exists( 'fr_image_uploader_field' ) ) {   
    fr_image_uploader_field( 'uploader_custom', get_post_meta($post->ID, 'uploader_custom',true), 134, 134,  get_stylesheet_directory_uri() . '/img/partners/no-image.jpg');
     wp_nonce_field( plugin_basename(__FILE__), 'fr_noncename' );
 
  }
}
function fr_image_field_print2($post) {
  if( function_exists( 'fr_image_uploader_field' ) ) {   
    fr_image_uploader_field( 'uploader_custom', get_post_meta($post->ID, 'uploader_custom',true), 150, 150,  get_stylesheet_directory_uri() . '/img/reviewers/no-image.jpg');
     wp_nonce_field( plugin_basename(__FILE__), 'fr_noncename' );
 
  }
} 

function fr_image_field_print3($post) {
  if( function_exists( 'fr_image_uploader_field' ) ) {   
    fr_image_uploader_field( 'uploader_custom', get_post_meta($post->ID, 'uploader_custom',true), 675, 380,  get_stylesheet_directory_uri() . '/img/blog/blog-img.jpg');
     wp_nonce_field( plugin_basename(__FILE__), 'fr_noncename' ); 
  }
} 

function fr_post_field_print($post)
{  
  $value = get_post_meta( $post->ID, 'fr_post', true );
  echo '<input type="text" id="fr_post" name="fr_post"';
        echo ' value="' . esc_attr( $value ) . '" size="25" />';
}

function fr_post_text_area ($post) {
  $value = get_post_meta($post->ID, 'fr_post_text', true);
  echo '<textarea name="fr_post_text" style="width:100%">'. esc_attr( $value ) .'</textarea>';
}


/*
 * Сохраняем данные произвольного поля
 */

function fr_save_postdata( $post_id ) {
  // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места. 
 if ( ! wp_verify_nonce( $_POST['fr_noncename'], plugin_basename(__FILE__) ) )
    return $post_id; 

  // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
    return $post_id;

  // проверяем разрешено ли пользователю указывать эти данные
  if( ! current_user_can( 'edit_post', $post_id ) ) {
    return $post_id;
  }
 
  // Если поле установлено.
  if (isset( $_POST['fr_post'] ) )
  {
    $my_data = sanitize_text_field( $_POST['fr_post'] );    
    // Обновляем данные в базе данных.
    update_post_meta( $post_id, 'fr_post', $my_data );
  }  
 
  if (isset( $_POST['fr_post_text'] ) )
  {
    $my_data = sanitize_text_field( $_POST['fr_post_text'] );    
    // Обновляем данные в базе данных.
    update_post_meta( $post_id, 'fr_post_text', $my_data );
  }  

  if (isset( $_POST['uploader_custom'] ) )
  {   
    update_post_meta( $post_id, 'uploader_custom', $_POST['uploader_custom']); 
  }    
}
 
add_action('save_post', 'fr_save_postdata');

/* Вывод изображений партнеров на главной */
function getPartnerImage($post) {
  
  $w = 134; $h = 134;
  
  $value = get_post_meta($post->ID, 'uploader_custom',true); 

  if( $value ) {
    $image_attributes = wp_get_attachment_image_src( $value, array($w, $h) );
    $src = $image_attributes[0];
     echo ' 
     <div class="slide">
        <div class="partners"> 
          <img src="' . $src . '" width="' . $w . 'px" height="' . $h . 'px" alt="'. $post->post_title .'"/>      
        </div>
     </div>
    ';
  } 
 
}

/* Вывод изображений партнеров на главной */
function getHeaderBlogImage($post) {
  
  $w = 675; $h = 380;
  
  $value = get_post_meta($post->ID, 'uploader_custom',true); 

  if( $value ) {
    $image_attributes = wp_get_attachment_image_src( $value, array($w, $h) );
    $src = $image_attributes[0];
     echo ' 
     <div class="img-anonse margin-bottom-20">
      <img class="img-responsive" src="' . $src . '" alt="'. $post->post_title .'" />
     </div>   
    ';
  } 
 
}

function getCommentAnons($string)
{
  $string = strip_tags($string);
  $string = substr($string, 0, 400);  
  $string = rtrim($string, "!,.-");
  $string = substr($string, 0, strrpos($string, ' '));
  echo $string."… ";
}

//Вывод отзывов на главной
function getComments($post, $close_row)
{
  $w = 150; $h = 150;
  $default = get_stylesheet_directory_uri() . '/img/reviewers/no-image.jpg';
  $meta = new stdClass;
  foreach( get_post_meta( $post->ID ) as $k => $v )
   $meta->$k = $v[0];
   $name = $post->post_title;   
   $image_attributes = wp_get_attachment_image_src($meta->uploader_custom, array($w, $h) );
   $src = '';
  
   if($meta->uploader_custom) {
      $image_attributes = wp_get_attachment_image_src( $meta->uploader_custom, array($w, $h) );
      $src = $image_attributes[0];
    } else {
      $src = $default;
    }
       
    echo '<div class="col-xs-12 col-sm-6">
          <div class="person">
            <img src="'. esc_url( $src ). '" class="img-responsive img-circle" width="75" alt="'.$name.'">

            <div class="person-info">
              <div class="font-x1 font-weight-600">'.$name.'</div>

              <div class="font-x1"><small>'.$meta->fr_post.'</small></div>
            </div>
            <div class="dialog margin-bottom-30">
              <div class="triangle-left"></div>
              <div class="triangle-top"></div>';
            getCommentAnons($meta->fr_post_text);      
            echo '</div><a class="view-all underline" href="';      
          // echo '</div><a class="thickbox view-all underline" href="';
           the_permalink(); 
           //echo '&v=iframe&?TB_iframe=true&width=600&height=100%">посмотреть</a>
           echo '">посмотреть</a>
          </div>
        </div>';

    if ($close_row)
    {
      echo '</div><div class="row">';
    }

     
}

//------------------
//Подключение меню
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

if (function_exists('add_theme_support'))  { 
add_theme_support('menus');
}

register_nav_menus( array(
  'primary' => 'Главное меню',
  'footer_menu' => 'Меню в подвале'
) );


//Подключение виджетов
//Контакты в шапке
function register_wp_sidebars() {
  register_sidebar(
    array(
      'id' => 'contact-header', // уникальный id
      'name' => 'Контакты в шапке', // название сайдбара
      'description' => '', // описание
      'before_widget' => '', // по умолчанию виджеты выводятся <li>-списком
      'after_widget' => '',
      'before_title' => '', // по умолчанию заголовки виджетов в <h2>
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-band-1', 
      'name' => 'Полоса лендинга 1', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-band-2', 
      'name' => 'Полоса лендинга 2', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-band-3', 
      'name' => 'Полоса лендинга 3', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );
  register_sidebar(
    array(
      'id' => 'landing-band-4', 
      'name' => 'Полоса лендинга 4', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );
  register_sidebar(
    array(
      'id' => 'landing-band-5', 
      'name' => 'Полоса лендинга 5', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );
  register_sidebar(
    array(
      'id' => 'landing-band-6', 
      'name' => 'Полоса лендинга 6', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );
  register_sidebar(
    array(
      'id' => 'landing-band-7', 
      'name' => 'Полоса лендинга 7', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-band-8', 
      'name' => 'Полоса лендинга 8', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-band-9', 
      'name' => 'Полоса лендинга 9', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-band-10', 
      'name' => 'Полоса лендинга 10', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  );

  register_sidebar(
    array(
      'id' => 'landing-menu', 
      'name' => 'Лендинг меню', 
      'description' => '', 
      'before_widget' => '', 
      'after_widget' => '',
      'before_title' => '', 
      'after_title' => ''
    )
  ); 
  
}
 
add_action( 'widgets_init', 'register_wp_sidebars' );

//custom more link
add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
return '<div class="text-right"><a class="btn btn-primary _btn-lg w-fix-150" href="'.get_permalink().'">Подробнее</a></div>';
}

//custom pagination
if ( ! function_exists( 'my_paging_nav' ) ) :
function my_paging_nav() {
  global $wp_query, $wp_rewrite;

  // Don't print empty markup if there's only one page.
  if ( $wp_query->max_num_pages < 2 ) {
    return;
  }

  $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
  $pagenum_link = html_entity_decode( get_pagenum_link() );
  $query_args   = array();
  $url_parts    = explode( '?', $pagenum_link );

  if ( isset( $url_parts[1] ) ) {
    wp_parse_str( $url_parts[1], $query_args );
  }

  $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
  $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

  $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
  $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

  // Set up paginated links.
  $links = paginate_links( array(
    'base'     => $pagenum_link,
    'format'   => $format,
    'total'    => $wp_query->max_num_pages,
    'current'  => $paged,
    'mid_size' => 1,
    'add_args' => array_map( 'urlencode', $query_args ),
    'prev_text' => '«',
    'next_text' => '»',
    'type' =>'array'
  ) );

  if ( $links ) :     
  ?>       
  <nav> 
  <ul class="pagination pagination-sm nomargin">
  <?php 
    foreach ( $links as $p ) {
      echo "<li >".$p."</li>";
    }  
  ?> 
  </ul>      
  </nav>
  <?php
  endif;
}
endif;

//Скрываем заголовок виджета
add_filter( 'widget_title', 'hide_widget_title' );
function hide_widget_title( $title ) {
    //if ( empty( $title ) ) return '';
   // if ( $title[0] == '!' ) return '';
    return '';
}

//Убираем загрузку jQuery из head
/*add_filter( 'wp_enqueue_scripts', 'change_default_jquery', PHP_INT_MAX );

function change_default_jquery( ){
    wp_dequeue_script( 'jquery');
    wp_deregister_script( 'jquery');   
}*/

//Популярные посты
function getPostViews($postID){
  $count_key = 'post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
    return "0 просмотров";
  }
  echo _e('просмотров ', 'dot-b');
  return $count;
}
 
function setPostViews($postID) {
  $count_key = 'post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
  }else{
    $count++;
    update_post_meta($postID, $count_key, $count);
  }
}
 
function getMostViews($limit)
{
  //if ( !isset($limit) ) $limit = 5;
  global $wpdb;
  $result = $wpdb->get_results("SELECT meta_value, post_title, ID FROM $wpdb->postmeta LEFT JOIN $wpdb->posts ON ($wpdb->postmeta.post_id = $wpdb->posts.ID) WHERE $wpdb->postmeta.meta_key = 'post_views_count' ORDER BY meta_value DESC LIMIT 5");
  return $result;
}
// ******************
//Разрешаем шорткоды в виджетах
add_filter('widget_text', 'do_shortcode');

//URL shortcode
function get_temp_url() {
  return get_template_directory_uri();
}
add_shortcode('url', 'get_temp_url');




//Отключаем комментарии в админ меню
add_action( 'admin_menu', 'remove_menu_items' );
 
function remove_menu_items() {    
  remove_menu_page('edit-comments.php'); // Удаляем пункт "Комментарии"
}

function my_rewrite_flush() {
    partners_init();
    comments_init();
    flush_rewrite_rules();
}
add_action('after_switch_theme', 'my_rewrite_flush');



?>