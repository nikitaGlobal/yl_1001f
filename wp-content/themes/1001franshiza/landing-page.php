<?php
/*
Template Name:  Лендинг
*/
?>

<?php
	get_header();
?>

	<!-- NAVIGATION LANDING -->

	<div class="navigation">
	 	<div class="container-fluid"> 
			<div class="container _nopadding">
				<nav id="navbar-scroll"> 
		 			<ul class="nav ul-clear menu text-center"> 
		 			<?php if ( is_active_sidebar( 'landing-menu' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-menu' ); ?>							
						<?php endif; ?>		
		 			</ul>
		 		</nav>
		 	</div>
		 </div>	 		
	 </div>

	<!-- BANNER + TITLE -->

	<div class="container-fluid bg-image-banner">
		<div class="container padding-height-40">
			<div class="row margin-bottom-20">
				<div class="col-xs-12 col-sm-6 margin-bottom-20 nopadding text-center text-white">
					<?php if ( is_active_sidebar( 'landing-band-1' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-1' ); ?>							
						<?php endif; ?>
				</div>

				<div class="col-xs-12 col-sm-6 margin-bottom-20 text-center">
					<div class="order">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gift.gif" alt="" width="190px" class="img-responsive">
						<div class="free margin-bottom-10">Бесплатно</div>
						<span class="label-for-button">Аудит вашего бизнеса</span>						
						<a href="#contact_form_pop" class="fancybox-inline btn btn-primary btn-lg w-100 margin-bottom-20">Заказать</a>
						<span class="label-for-button">Инструкция по работе</span>						
						<a href="#contact_form_pop" class="fancybox-inline btn btn-primary btn-lg w-100 margin-bottom-20">Получить</a> 
						<a class="fancybox-inline link underline" href="#contact_form_pop">Консультация специалиста</a>
					</div>
				</div>
			</div>			
			<div class="row margin-bottom-20">
				<div class="cols3">
					<img src="<?php echo get_template_directory_uri(); ?>/img/1.gif" alt="" class="img-responsive img-circle">
					<div>Ваш опыт</div>
				</div>
				<div class="cols3-num">+</div>
				<div class="cols3">
					<img src="<?php echo get_template_directory_uri(); ?>/img/2.gif" alt="" class="img-responsive img-circle">
					<div>Наши знания</div>
				</div>
				<div class="cols3-num">=</div>
				<div class="cols3">
					<img src="<?php echo get_template_directory_uri(); ?>/img/3.gif" alt="" class="img-responsive img-circle">
					<div>Ваш пассивный доход</div>
				</div>
			</div>
			<div class="row text-center">
				<a href="#contact_form_pop" class="fancybox-inline button-create">
					Создай и продай
					<span>Франшизу</span>
					<img src="<?php echo get_template_directory_uri(); ?>/img/galka.png" alt="">
				</a>
			</div>
		</div>
	</div>

	<!-- Сколько вы можете зарабатывать? ARRIVAL--> 
	
	<div class="container-fluid bg-silverLight box-shadow" id="arrival">
		<div class="container padding-height-40">

			<div class="row padding-lr-20">
				<div class="inform margin-bottom-30">
					<?php if ( is_active_sidebar( 'landing-band-2' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-2' ); ?>							
						<?php endif; ?>
				</div>
			</div>
			<h1 class="margin-bottom-30 text-center">Сколько Вы сможете зарабатывать,<br>
если сделаете свою франшизу?</h1>
			<div class="row">
				<div class="hidden-xs col-sm-3">
					<div class="box-round-15 overflow square">
						<img src="<?php echo get_template_directory_uri(); ?>/img/scheti.gif" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-9 _nopadding">
					<!-- 1 ============== -->
					<div class="row margin-bottom-10">
						<div class="xs-mobile-12 col-xs-8 col-sm-8">
							<div class="polzunok">
								<div class="zag">Размер паушального взноса</div>

								<input class="slider" type="text" data-slider-min="250000" data-slider-max="850000" data-slider-tooltip="hide" data-slider-step="1000" data-slider-value="250000"/>								
							</div>
						</div>
						<div class="xs-mobile-12 col-xs-4 col-sm-4 margin-top-30">
							<div class="price-view"><span id="fee" class="price">250 000</span><span class="fa fa-rub"></span></div>
						</div>		
					</div>
					<!-- 2 ================= -->
					<div class="row">
						<div class="xs-mobile-12 col-xs-8 col-sm-8">
							<div class="polzunok">
								<div class="zag">Сумма роялти</div>

								<input class="slider" type="text" data-slider-min="50000" data-slider-max="250000" data-slider-tooltip="hide" data-slider-step="1000" data-slider-value="50000"/>								
							</div>
						</div>
						<div class="xs-mobile-12 col-xs-4 col-sm-4 margin-top-30">
							<div class="price-view">
								<span id="royalty" class="price">50 000</span><span class="fa fa-rub"></span>
							</div>
						</div>		
					</div>
				</div>				
			</div>
			<div class="arrow_box _triangle-down"></div>
		</div>

	</div>

	<div class="container-fluid bg-silver margin-bottom-30">
		<div class="container padding-height-40">
			<div class="itogo margin-bottom-30">
				Доход в год при продаже всего одной франшизы
				<span class="price-itog">
					<span id="res1" class="price">-</span><span class="fa fa-rub"></span>
				</span>
				
			</div>
			<div class="itogo">
				Доход в год при продаже всего одной франшизы каждые 3 месяца
				<span class="price-itog">
					<span id="res2" class="price">-</span><span class="fa fa-rub"></span>	
				</span>				
			</div>
		</div>
	</div>

	<!-- ВАЖНО ============ -->

	<div class="container-fluid bg-red margin-bottom-30">
		<div class="container">
			<div class="row padding-lr-20">
				<div class="red-plane _margin-bottom-30">
					<?php if ( is_active_sidebar( 'landing-band-3' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-3' ); ?>							
						<?php endif; ?>			
				</div>
			</div>
		</div>
	</div>

	<!-- ПОЧЕМУ МЫ ? -->

	<div class="container-fluid bg-secondary-dark">
		<div class="container nopadding padding-height-40">
			<?php if ( is_active_sidebar( 'landing-band-4' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-4' ); ?>							
						<?php endif; ?>	
		</div>
	</div>

	<!-- УСЛУГИ И ЦЕНЫ -->

	<div class="container-fluid bg-brand-textura overflow" id="prices">
		<div class="container _nopadding padding-top-40 margin-bottom-20">
			<?php if ( is_active_sidebar( 'landing-band-5' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-5' ); ?>							
						<?php endif; ?>	
		</div>
	</div>

	<!-- ВАЖНО ============ -->

	<div class="container-fluid bg-red">
		<div class="container">
			<div class="row padding-lr-20">
				<div class="red-plane">
					<?php if ( is_active_sidebar( 'landing-band-6' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-6' ); ?>							
						<?php endif; ?>		
				</div>
			</div>			
		</div>
	</div>

	<!-- ZAKAZ -->

	<div class="container-fluid bg-white box-shadow-big">
		<div class="container padding-height-40">
			<div class="row">
				<?php if ( is_active_sidebar( 'landing-band-7' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-7' ); ?>							
						<?php endif; ?>	
			</div>
		</div>
	</div>

	<!-- TEST -->

	<div class="container-fluid bg-test">
		<div class="container nopadding padding-height-40">
			<?php if ( is_active_sidebar( 'landing-band-8' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-8' ); ?>							
						<?php endif; ?>			
		</div>
	</div>

	<!-- СТРУКТУРА -->

	<div class="container-fluid bg-brand-textura-2" id="how-work">
		<div class="container nopadding padding-height-40">
			<?php if ( is_active_sidebar( 'landing-band-9' ) ) : ?>							
								<?php dynamic_sidebar( 'landing-band-9' ); ?>							
						<?php endif; ?>	
		</div>
	</div>


	<!-- НАШИ ПАРТНЕРЫ -->
	
	<div class="container-fluid bg-silverLight">
		<div class="container padding-height-40">
			<h1 class="margin-bottom-30 text-center">Наши партнеры</h1>
			<div id="bxslider">

			<?php	
				$args =  array(
				'posts_per_page'  =>  '-1',
				'post_type'  =>  'partners'
				);
				$partners = new WP_Query( $args );	
				while ( $partners->have_posts() )  :  $partners->the_post();			
					getPartnerImage($post); 			
				endwhile;	
				wp_reset_postdata();
			?>		
			 	  
			</div>
		</div>
	</div>


	<!-- ОТЗЫВЫ -->
<?php /*add_thickbox(); */?>
	<div class="container-fluid bg-white box-shadow-big z-index-200" id="reviews">
		<div class="container padding-height-40">
			<h1 class="margin-bottom-30 text-center">Отзывы</h1>
			<?php
				$args =  array(
				'posts_per_page'  =>  '4',
				'post_type'  =>  'comments'
				);
				$comments = new WP_Query( $args );	
				$row_count=$comments->found_posts;

     			$close_row=true;

     			echo '<div class="row">';    
				$i=0;
				while ( $comments->have_posts() )  :  $comments->the_post();					
					$i++;
					$close_row=(!$close_row) && ($i<>$row_count);
					getComments($post,$close_row);						
				endwhile;	
				wp_reset_postdata();
			    
			    echo '</div>';
			?>	
			
		</div>
	</div>

	<!-- CONTACT -->
	<?php get_template_part('page-contact'); ?>	

	
<!-- END CONTENT -->

<!-- INCLUDE SCRIPTS -->
	 
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>    
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>  
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-slider.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script> 
	<script src="<?php echo get_template_directory_uri(); ?>/js/accounting.min.js"></script>
	
	<script>
		var $ = jQuery.noConflict();
		$(document).ready(function() {
			var navigation = $('.navigation');
		  	var navs = navigation.find('a');  
		  	var navTop = navigation.offset().top;	
		  	var windowTop = $(document).scrollTop();
  			var windowWidth = $(window).width();
  			var windowHeight = $(window).height();  	  	

		  	menuTop(); 

		  	function menuTop() {
				if (windowTop >= navTop) {
			    	navigation.addClass('top-fix');
			      	if ( windowWidth >= 805 ) { 
			        	$('.header').css({marginBottom: navigation.height() }); // плавность прыжка
			      	} else { $('.header').css({marginBottom: '0' }); }
			    }

			    if (windowTop <= navTop) {
			    	navigation.removeClass('top-fix'); 
			      	$('.header').css({marginBottom: '0' });
			    }
			} 


		  	$(window).resize(function() {
		    	navTop = navigation.offset().top;   
		    	windowWidth = $(this).width();    
		  	});

		  	$(window).scroll(function() {
    
				// прилепить меню к верху
			    windowTop = $(document).scrollTop();   
			    menuTop();			 
			}); 

		  	// плавность прокрутки
  
			navs.click(function() {
		      $("html, body").stop().animate({
		         scrollTop: $($(this).attr("href")).offset().top - 27 + "px"
		      }, {
		         duration: 550,
		         easing: "swing"
		      });
		      
		      return false;
		   	});

		   	//-----------------------------------------------
			
			var slider = $('.slider');
			slider.slider();							
			calcProfit();	
			
			slider.on("slide", function(slideEvt) {		
				var unf = accounting.unformat(slideEvt.value);
				var value=accounting.formatNumber(parseInt(unf), 0, " ");
				$(this).parent().parent().parent().find('.price-view').find('.price').text(value);				
			});

			slider.on("change", function() {				
				calcProfit();						
			});
			

			function calcProfit()
			{
				var fee=accounting.unformat($('#fee').text());
				var royalty=accounting.unformat($('#royalty').text());		
				var res1=(parseInt(royalty)*12)+parseInt(fee);	
				res1=accounting.formatNumber(res1, 0, " ");		
				$('#res1').text(res1);				
				var res2=parseInt(royalty)*12+parseInt(royalty)*9+parseInt(royalty)*6+parseInt(royalty)*3+parseInt(fee)*4;
				res2=accounting.formatNumber(res2, 0, " ");
				$('#res2').text(res2);				
			};

			


			$('#bxslider').bxSlider({
				auto: true,
				controls: false,
				infiniteLoop: true,
			    slideWidth: 145,
			    minSlides: 2,
			    maxSlides: 6,
			    moveSlides: 6,
			    slideMargin: 12
			  });
		});
	</script>

<!-- END INCLUDE SCRIPTS -->

</body>
</html>
<?php wp_footer(); ?>