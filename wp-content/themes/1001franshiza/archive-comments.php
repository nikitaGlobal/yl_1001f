
<?php get_header();  ?>	
	
	<div class="container-fluid bg-silverLight box-shadow-big z-index-100 overflow">
		<div class="container nopadding padding-top-40">			
			<div class="row nomargin">		
				<div class="col-xs-12 col-sm-12 margin-bottom-30">					
						<h1 class="margin-bottom-30 text-center">Отзывы</h1>

						<?php 
						 $w = 150; $h = 150;
 						 $default = get_stylesheet_directory_uri() . '/img/reviewers/no-image.jpg';

						while ( have_posts() ) : the_post(); ?>

						<?php 
							$meta = new stdClass;
							  foreach( get_post_meta( $post->ID ) as $k => $v )
							   $meta->$k = $v[0];
							   $name = $post->post_title;   
							   $image_attributes = wp_get_attachment_image_src($meta->uploader_custom, array($w, $h) );
							   $src = '';
							  
							   if($meta->uploader_custom) {
							      $image_attributes = wp_get_attachment_image_src( $meta->uploader_custom, array($w, $h) );
							      $src = $image_attributes[0];
							    } else {
							      $src = $default;
							    }
						 ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
					          <div class="person">
					            <img width="75" alt="<?php echo $name; ?>" class="img-responsive img-circle" src="<?php echo esc_url($src); ?>">

					            <div class="person-info">
					              <div class="font-x1 font-weight-600"><?php echo $name; ?></div>

					              <div class="font-x1"><small><?php echo $meta->fr_post; ?></small></div>
					            </div>
					            <div class="dialog margin-bottom-30">
					              <div class="triangle-left"></div>
					              <div class="triangle-top"></div><?php getCommentAnons($meta->fr_post_text); ?></div>
					              <a href="<?php the_permalink(); ?>" class="view-all underline">посмотреть</a>
					          </div>
					        </div>
						</div>
						<?php endwhile; ?>

					<?php my_paging_nav(); ?>
				</div>		

			</div>
			
		</div>
	</div>

	<!-- FOOTER -->
<?php	
	   get_footer();	
?>
