<?php 
$iframe=false;
$str='iframe'; 
$url=esc_url($_SERVER["REQUEST_URI"]);
foreach (explode('&', $url) as $chunk) {
    $param = explode("=", $chunk);
    if ($str==$param[1]) {
    	$iframe=true;
    }
}
?>

<?php if ($iframe): ?>
		<!DOCTYPE html>
		<html <?php language_attributes(); ?>>
		<head>
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
		    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
			<?php endif; ?>
			
			<title><?php bloginfo('name'); ?> | 
			<?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
			<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-slider.min.css" type="text/css">
		    <link href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" rel="stylesheet">

		    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css" rel="stylesheet">          

		    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		    <!--[if lt IE 9]>
		      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			   <script src="<?php echo get_template_directory_uri(); ?>/js/less.js" type="text/javascript"></script>
		    <![endif]-->
		    <?php wp_head(); ?>
		</head>
		<body>
	<?php else : ?>	
	<?php get_header();  ?>	
	<?php endif; ?>

	
	<div class="container-fluid bg-silverLight box-shadow-big z-index-100 overflow">
		<div class="container nopadding padding-top-40">			
			<div class="row nomargin">		
				<div class="col-xs-12 col-sm-12 margin-bottom-30">					
						<h1 class="margin-bottom-30 text-center">Отзыв</h1>

						<?php
						 $w = 150; $h = 150;
 						 $default = get_stylesheet_directory_uri() . '/img/reviewers/no-image.jpg';
						// Start the Loop.
						while ( have_posts() ) : the_post();	

			     			echo '<div class="row">';   
							 
							  $meta = new stdClass;
							  foreach( get_post_meta( $post->ID ) as $k => $v )
							   $meta->$k = $v[0];
							   $name = $post->post_title;   
							   $image_attributes = wp_get_attachment_image_src($meta->uploader_custom, array($w, $h) );
							   $src = '';
							  
							   if($meta->uploader_custom) {
							      $image_attributes = wp_get_attachment_image_src( $meta->uploader_custom, array($w, $h) );
							      $src = $image_attributes[0];
							    } else {
							      $src = $default;
							    }
							    
							    echo '<div class="col-xs-12 col-sm-12">
							          <div class="person">
							            <img src="'. esc_url( $src ). '" class="img-responsive img-circle" width="75" alt="'.$name.'">

							            <div class="person-info">
							              <div class="font-x1 font-weight-600">'.$name.'</div>
							              <div class="font-x1"><small>'.$meta->fr_post.'</small></div>
							            </div>
							            <div class="dialog margin-bottom-30">
							              <div class="triangle-left"></div>
							              <div class="triangle-top"></div>'.
							              $meta->fr_post_text.'            
							            </div>
							          </div>
							        </div>';							
									wp_reset_postdata();
						    
						    echo '</div>';													
						endwhile;
						?>														
				</div>		

			</div>			
		</div>
	</div>

	<!-- FOOTER -->
<?php
	if ($iframe) 
	{ 
		echo '
		</body>
		</html>
		';
		
	}
	else
	{
	   get_footer();
	}	
?>
