<?php
	get_header();
?>
	
	<div class="container-fluid bg-image-blog box-shadow-big z-index-100 overflow">
		<div class="container nopadding padding-top-40">			
			<div class="row nomargin">		
				<div class="col-xs-12 col-sm-12 margin-bottom-30">					
						<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();							
							get_template_part( 'content', 'page' );							
						endwhile;
						?>														
				</div>		

			</div>
			<div class="star-1-blog"></div>
			<div class="star-2-blog"></div>
		</div>
	</div>

	<!-- FOOTER -->
<?php
	get_footer();
?>