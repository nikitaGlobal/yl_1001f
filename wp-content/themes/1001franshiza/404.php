<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>1001 Franshiza - 404</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
	<link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" type="text/css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css" rel="stylesheet">          

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	   <script src="<?php echo get_template_directory_uri(); ?>/js/less.js" type="text/javascript"></script>
    <![endif]-->
</head>
<body>

	<div class="container-fluid error-back">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="error-page">
						<div class="oops">
							OOPS 
							<span class="icon">
								<i class="fa fa-smile-o fa-rotate-270"></i>
							</span>
						</div>

						<div class="e404">404</div>

						<div class="t">такой страницы не существует</div>

						<a href="<?php echo get_home_url(); ?>" class="main">
							<div>перейти<br>на главную</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/star-button.gif" alt="">
						</a>						
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>	